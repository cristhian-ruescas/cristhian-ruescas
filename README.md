## Sejam Bem-vindos ao meu perfil !!! <img src="https://www.imagensanimadas.com/data/media/318/emoticon-e-smiley-computador-imagem-animada-0031.gif" border="0" alt="emoticon-e-smiley-computador-imagem-animada-0031" /></a>
<div align="center">
  <a href="https://github.com/cristhian-ruescas">
  <img height="150em" src="https://github-readme-stats.vercel.app/api?username=Cristhian-Ruescas&show_icons=true&theme=blue&include_all_commits=true&count_private=true"/>
  <img height="150em" "180em" src="https://github-readme-stats.vercel.app/api/top-langs/?username=Cristhian-Ruescas&layout=compact&langs_count=7&theme=blue"/>
</div>
<div style="display: inline_block"><br>
 
  
  <img align="center" alt="Cris-Kollin" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/kotlin/kotlin-plain.svg">
  <img align="center" alt="Cris-Java" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/java/java-plain.svg">
  <img align="center" alt="Cris-Python" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/python/python-original.svg">
  <img align="center" alt="C-Spring" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/spring/spring-original.svg">
  <img align="center" alt="C-git" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/github/github-original.svg">
       

  <img align="right" alt="Cris-pic" height="200" style="border-radius:50px;" 
       src="https://cdn.discordapp.com/attachments/892829776732114968/904551197346766908/ezgif-4-4c09f32fc57c.gif">
</div>
  
  ##
 
<div> 
  <a href="https://www.instagram.com/cris_0_029/" target="_blank"><img src="https://img.shields.io/badge/-Instagram-%23E4405F?style=for-the-badge&logo=instagram&logoColor=white" target="_blank"></a>
  <a href = "cristhian.ruescas83@gmail.com><img src="https://img.shields.io/badge/-Gmail-%23333?style=for-the-badge&logo=gmail&logoColor=white" target="_blank"></a>
  <a href="https://www.linkedin.com/in/cristhianruescas/" target="_blank"><img src="https://img.shields.io/badge/-LinkedIn-%230077B5?style=for-the-badge&logo=linkedin&logoColor=white" target="_blank"><a href="https://www.imagensanimadas.com/cat-emoticons-e-smileys-de-computadores-318.htm">
    <a href="https://api.whatsapp.com/send?1=pt_BR&phone=11959586681&text=Olá, sou a pessoa do github!"> 
    <img alt="Phone - Cristhian Ruescas" src="https://img.shields.io/badge/WhatsApp-25D366?style=for-the-badge&logo=whatsapp&logoColor=white">
  </a>  

### 👨 Sobre mim 
Bom falando um pouco sobre mim, entrei a pouco tempo na área de programação mas me apaixonei de cara, foi então que decidi fazer faculdade de Ciências da Computação no qual curso até hoje na São Judas Tadeu. Depois de alguns meses depois, eu acabei entrando em um bootcamp chamado generation, no qual aprendi Kotlin, e ao finalizar fui contratado pela Dasa, como desenvolvedor mobile, kotlin e swift.

> Uma das minhas filosofias : Educação é a arma mais poderosa que você pode usar para mudar o mundo.

 🔌🌎
 

 
![Snake animation](https://github.com/cristhian-ruescas/cristhian-ruescas/blob/output/github-contribution-grid-snake.svg)
 
</div>
